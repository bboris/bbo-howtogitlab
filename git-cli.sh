#!/bin/bash
 
# create backup gitlab /var/opt/gitlab/backups
gitlab-rake gitlab:backup:create


# do restore gitlab /var/opt/gitlab/backups, if backup is not on that location you must copy to backup loaction
sudo cp 11493107454_2018_04_25_10.6.4-ce_gitlab_backup.tar /var/opt/gitlab/backups/
sudo chown git.git /var/opt/gitlab/backups/11493107454_2018_04_25_10.6.4-ce_gitlab_backup.tar

# steps for restoring backup
## first stop unicorn and sidekiq
sudo gitlab-ctl stop unicorn
sudo gitlab-ctl stop sidekiq
# Verify
sudo gitlab-ctl status

# This command will overwrite the contents of your GitLab database!
sudo gitlab-rake gitlab:backup:restore BACKUP=1493107454_2018_04_25_10.6.4-ce

# Restart gitlab 
sudo gitlab-ctl restart
sudo gitlab-rake gitlab:check SANITIZE=true




